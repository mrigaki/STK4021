library(ggplot2)
library(coda)
library(xtable)
# Set prior parameters
m0 <- 7
g02 <- 5
t02 <- 10
eta0 <- 2
s02 <- 15
n0 <- 2

n<- sv <- ybar <- array(NA, 8)

Y1 <- read.table("/home/emarrig/repos/STK4021/project/school1.dat")
n[1] <- dim(Y1)[1]
sv[1] <- var(Y1)
ybar[1] <- mean(Y1$V1)

Y2 <- read.table("/home/emarrig/repos/STK4021/project/school2.dat")
n[2] <- dim(Y2)[1]
sv[2] <- var(Y2)
ybar[2] <- mean(Y2$V1)

Y3 <- read.table("/home/emarrig/repos/STK4021/project/school3.dat")
n[3] <- dim(Y3)[1]
sv[3] <- var(Y3)
ybar[3] <- mean(Y3$V1)

Y4 <- read.table("/home/emarrig/repos/STK4021/project/school4.dat")
n[4] <- dim(Y4)[1]
sv[4] <- var(Y4)
ybar[4] <- mean(Y4$V1)

Y5 <- read.table("/home/emarrig/repos/STK4021/project/school5.dat")
n[5] <- dim(Y5)[1]
sv[5] <- var(Y5)
ybar[5] <- mean(Y5$V1)

Y6 <- read.table("/home/emarrig/repos/STK4021/project/school6.dat")
n[6] <- dim(Y6)[1]
sv[6] <- var(Y6)
ybar[6] <- mean(Y6$V1)

Y7 <- read.table("/home/emarrig/repos/STK4021/project/school7.dat")
n[7] <- dim(Y7)[1]
sv[7] <- var(Y7)
ybar[7] <- mean(Y7$V1)

Y8 <- read.table("/home/emarrig/repos/STK4021/project/school8.dat")
n[8] <- dim(Y8)[1]
sv[8] <- var(Y8)
ybar[8] <- mean(Y8$V1)

Y <- read.csv("/home/emarrig/repos/STK4021/project/schools.csv", header = FALSE)
Y[is.na(Y)] <- 0

m <- 8 # 8 schools
K <- 5000 # Number of iterations

mus <- array(NA, dim=c(K,1))
taus <- array(NA, dim=c(K,1))
sigmas <- array(NA, dim=c(K,1))
thetas <- array(NA, dim=c(K,m))

# Initial values for the sampler
thetas[1, ] <- ybar
mus[1] <- mean(thetas[1, ])
taus[1] <- var(thetas[1, ])
sigmas[1] <- mean(sv)

for (k in 1:(K-1)) {
  
  # Sample mu
  denom <- m/taus[k]+1/g02
  mmu <- (m*mean(thetas[k, ])/taus[k] + m0/g02)/denom
  mus[k+1] <- rnorm (1, mmu, sqrt(1/denom))
  
  # Sample tau2
  taus[k+1] <-1/rgamma (1, (eta0+m)/2, eta0*t02 + sum((thetas[k, ]-mus[k+1])^2)/2)
  
  # Sample sigma
  ss <- n0*s02
  for (j in 1:m) {
    ss <- ss+sum( (Y[j, k] - thetas[k, j])^2)
  }
  sigmas[k+1] <- 1/rgamma (1,  (n0+sum(n))/2, ss/2)
  
  # Sample thetas
  for(j in 1:m) {
    denom <- (n[j]/sigmas[k+1]+1/taus[k+1])
    mtheta <- (ybar[j]*n[j] / sigmas[k+1]+mus[k+1]/taus[k+1] )/ denom
    thetas[k+1, j] <- rnorm(1, mtheta , sqrt(1/denom))
  }
}

bayes.thetas <- array(NA, dim=c(8, 1))
for (i in 1:8) {
  bayes.thetas[i] <- mean(thetas[, i])
}
bayes.mu <- mean(mus)
bayes.tau <- mean(taus)
bayes.sigma <- mean(sigmas)

quantile(mus,c(0.025,0.975)) 
quantile(taus,c(0.025,0.975))
quantile(sigmas,c(0.025,0.975))

p1 <- qplot(seq(1, length(mus[50:K])), mus[50:K], xlab="Iteration", ylab=expression(mu))
p2 <- qplot(seq(1, length(sigmas[50:K])), sigmas[50:K], xlab="Iteration", ylab=expression(sigma^2))
p3 <- qplot(seq(1, length(taus[50:K])), taus[50:K], xlab="Iteration", ylab=expression(tau^2))
multiplot(p1, p2, p3, cols=3)

p1 <- qplot(seq(1, length(thetas[, 1])), thetas[, 1], xlab="Iteration", ylab="theta1")
p2 <- qplot(seq(1, length(thetas[, 2])), thetas[, 2], xlab="Iteration", ylab="theta2")
p3 <- qplot(seq(1, length(thetas[, 3])), thetas[, 3], xlab="Iteration", ylab="theta3")
p4 <- qplot(seq(1, length(thetas[, 4])), thetas[, 4], xlab="Iteration", ylab="theta4")
p5 <- qplot(seq(1, length(thetas[, 5])), thetas[, 5], xlab="Iteration", ylab="theta5")
p6 <- qplot(seq(1, length(thetas[, 6])), thetas[, 6], xlab="Iteration", ylab="theta6")
p7 <- qplot(seq(1, length(thetas[, 7])), thetas[, 7], xlab="Iteration", ylab="theta7")
p8 <- qplot(seq(1, length(thetas[, 8])), thetas[, 8], xlab="Iteration", ylab="theta8")
multiplot(p1, p2, p3, p4, p5, p6, p7, p8, cols=2)


# Check autocorrelation

par(mfrow=c(1,3))
acf(mus) -> a1
acf(sigmas) -> a2
acf(taus) -> a3

mean(a1$acf)
mean(a2$acf)
mean(a3$acf)

# Check effective size 
effectiveSize(mus)
effectiveSize(sigmas)
effectiveSize(taus)
effectiveSize(thetas[, 1])
effectiveSize(thetas[, 2])
effectiveSize(thetas[, 3])
effectiveSize(thetas[, 4])
effectiveSize(thetas[, 5])
effectiveSize(thetas[, 6])
effectiveSize(thetas[, 7])
effectiveSize(thetas[, 8])


plot(density(thetas[, 1]))
plot(density(thetas[, 2]))
plot(density(thetas[, 3]))
plot(density(thetas[, 4]))
plot(density(thetas[, 5]))
plot(density(thetas[, 6]))
plot(density(thetas[, 7]))
plot(density(thetas[, 8]))

par(mfrow=c(1,1))
# Plot densities for prior and posterior
mu.prior <- rnorm(10000, m0, sqrt(5))
dmf <- as.data.frame(mus[50:K])
dmf2 <- as.data.frame(mu.prior)
names(dmf) <- c("mus")

p1 <- ggplot() + geom_density(aes(x=mu.prior), colour="red", data=dmf2) + 
  geom_density(aes(x=mus), colour="black", data=dmf) +
  xlab(expression(mu)) + geom_vline(xintercept=bayes.mu, col='gray', linetype = "longdash")

tau.prior <- 1./rgamma(10000, 1, 10)
dtf <- as.data.frame(taus)
dtf2 <- as.data.frame(tau.prior)
names(dmf) <- c("taus")

p2 <- ggplot() + geom_density(aes(x=tau.prior), colour="red", data=dtf2) + 
  geom_density(aes(x=taus), colour="black", data=dtf) +
  xlab(expression(tau^2)) + geom_vline(xintercept=bayes.tau, col='gray', linetype = "longdash") +
  xlim(0, 18)

sigma.prior <- 1./rgamma(10000, 1, 15)
dsf <- as.data.frame(sigmas)
dsf2 <- as.data.frame(sigma.prior)
names(dmf) <- c("sigmas")

p3 <- ggplot() + geom_density(aes(x=sigma.prior), colour="red", data=dsf2) + 
  geom_density(aes(x=sigmas), colour="black", data=dsf) +
  xlab(expression(sigma^2)) + geom_vline(xintercept=bayes.sigma, col='gray', linetype = "longdash") +
  xlim(0, 2)

multiplot(p1, p2, p3, cols=3)

## Code for question (d)

df2 <- merge(Y1, Y2, all = T)
df2 <- merge(df2, Y3, all=T)
df2 <- merge(df2, Y4, all=T)
df2 <- merge(df2, Y5, all=T)
df2 <- merge(df2, Y6, all=T)
df2 <- merge(df2, Y7, all=T)
df2 <- merge(df2, Y8, all=T)

N <- dim(df2)[1]

# Pooled population variance
sigma_hat <- sum((n-1)*sv)/sum(n)

# Population mean
mu_hat <- sum(colSums(Y, na.rm=TRUE))/sum(n)

# Calculate s2 over all values
sigmas_hat <- sum((df2$V1-mu_hat)^2)/N

# Calculate tau2 
tau_hat <- max(0, sigmas_hat - sigma_hat)

# Calculate omega_hat
omega_hat <- sigma_hat/(sigma_hat+tau_hat)

# Calculate thetas
emp.thetas <- array(NA, dim=c(m, 1))
for (i in 1:m) {
  #emp.thetas[i] <- mu_hat + (1-omega_hat)*(ybar[i] - mu_hat)
  emp.thetas[i] <- mu_hat*omega_hat + (1-omega_hat)*ybar[i]
}

# Create the table for the report
thetas.df <- t(rbind(as.data.frame(ybar)$ybar, bayes.thetas[,1], emp.thetas[,1]))
colnames(thetas.df) <- c("Sample", "Bayes", "EB")
options(xtable.comment = FALSE)
options(xtable.booktabs = TRUE)
xtable(thetas.df, caption = "Beta estimates from OLS and Bayes and Bayes quantiles")

## Code for question (e)

dfom <- as.data.frame(ss0/(tt0+ss0))
colnames(dfom) <- c("prior")

# Calculation based on the sampler
omegas = sigmas/(taus+sigmas)
plot(density(omegas), xlim=c(0,0.1))
omega_hat.post <- mean(omegas)
dfom2 <- as.data.frame(omegas)

ggplot() + geom_density(aes(x=prior), colour="red", data=dfom) + 
  geom_density(aes(x=V1), colour="black", data=dfom2) +
  xlab(expression(omega)) + geom_vline(xintercept=omega_hat.post, col='green', linetype = "longdash") +
  geom_vline(xintercept=omega_hat, col='blue', linetype = "longdash") +
  xlim(0, 1.2)

## Code for question (f)

bays.res6 <- sum(thetas[, 7] < thetas[, 6])/K
bays.res5 <- sum(thetas[, 7] < thetas[, 5])/K
bays.res4 <- sum(thetas[, 7] < thetas[, 4])/K
bays.res3 <- sum(thetas[, 7] < thetas[, 3])/K
bays.res2 <- sum(thetas[, 7] < thetas[, 2])/K
bays.res1 <- sum(thetas[, 7] < thetas[, 1])/K
bays.res8 <- sum(thetas[, 7] < thetas[, 8])/K

bays.all <- bays.res1*bays.res2*bays.res3*bays.res4*bays.res5*bays.res6*bays.res8

emp.theta.d7 <-  rnorm(1000, emp.thetas[7], (1-omega_hat)*sigma_hat)
emp.theta.d6 <-  rnorm(1000, emp.thetas[6], (1-omega_hat)*sigma_hat)
emp.theta.d5 <-  rnorm(1000, emp.thetas[5], (1-omega_hat)*sigma_hat)
emp.theta.d4 <-  rnorm(1000, emp.thetas[4], (1-omega_hat)*sigma_hat)
emp.theta.d3 <-  rnorm(1000, emp.thetas[3], (1-omega_hat)*sigma_hat)
emp.theta.d2 <-  rnorm(1000, emp.thetas[2], (1-omega_hat)*sigma_hat)
emp.theta.d8 <-  rnorm(1000, emp.thetas[8], (1-omega_hat)*sigma_hat)
emp.theta.d1 <-  rnorm(1000, emp.thetas[1], (1-omega_hat)*sigma_hat)

emp.res6 <- sum(emp.theta.d7 < emp.theta.d6)/1000
emp.res5 <- sum(emp.theta.d7 < emp.theta.d5)/1000
emp.res4 <- sum(emp.theta.d7 < emp.theta.d4)/1000
emp.res3 <- sum(emp.theta.d7 < emp.theta.d3)/1000
emp.res2 <- sum(emp.theta.d7 < emp.theta.d2)/1000
emp.res1 <- sum(emp.theta.d7 < emp.theta.d1)/1000
emp.res8 <- sum(emp.theta.d7 < emp.theta.d8)/1000

emp.all <- emp.res1*emp.res2*emp.res3*emp.res4*emp.res5*emp.res6*emp.res8

# Multiple plot function
#
# ggplot objects can be passed in ..., or to plotlist (as a list of ggplot objects)
# - cols:   Number of columns in layout
# - layout: A matrix specifying the layout. If present, 'cols' is ignored.
#
# If the layout is something like matrix(c(1,2,3,3), nrow=2, byrow=TRUE),
# then plot 1 will go in the upper left, 2 will go in the upper right, and
# 3 will go all the way across the bottom.
#
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  require(grid)
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}

