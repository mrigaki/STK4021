---
title: "Written Project Exam for STK 4021"
author: "Candidate no: 342"
output:
  rmarkdown::tufte_handout:
    keep_tex: true
---




## Short summary
The work for this project was created using R, Rstudio and R markdown and specifically the Tufte template that allows the creation of pdf files based on \LaTeX\ and R as well as the inline insertion of code and plots, hopefully the result is fairly readable.
Please note that only the most important parts of the code are shown inline in the report. The full listings are presented in the appendix.


## References

1. Course notes
2. Carlin and Louis (2009): Bayesian methods for data analysis (Third edition), Chapman & Hall, ISBN  978-1-58488-697-6
3. Robert and Casella (2010):  Introducing Monte Carlo Methods with R, Springer ISBN: 978-1-4419-1575-7 (Print) 978-1-4419-1576-4 (Online)
4. Multiplot function that was used to produce multiple plots in one figure was taken from "http://www.cookbook-r.com/Graphs/Multiple_graphs_on_one_page_%28ggplot2%29/"

\pagebreak

# Problem 1 (hierarchical modeling)

## (a) Posterior distributions
The posterior distributions for the parameters $\boldsymbol{\theta}, \sigma^{2}, \mu, \tau^{2}$ are given in the notes [^notes], pages 33 and 34. 

$$ \mu|\theta, \tau^{2} \sim N(\frac{\frac{m\bar{\theta} }{\tau^{2}} + \frac{\mu_{0}}{\gamma_{0}^{2}}}{\frac{m}{\tau^{2}} + \frac{1}{\gamma_{0}^{2}}}, \left [ \frac{m}{\tau^{2}} + \frac{1}{\gamma_{0}^{2}}\right ]^{-1})$$,
$$ \frac{1}{\tau^{2}} | \theta, \mu \sim \Gamma (\frac{\eta_{0}+m}{2}, \frac{\eta_{0}\tau^{2}_{0}+ \sum_{j=1}^{m}(\theta_{j-}\mu)^{2}}{2)})$$
$$\theta_{j}|D^{(j)}, \sigma^{2} \sim N({\frac{n_{j}\bar{x}^{(j)}+\frac{\mu}{\tau^{2}}}{\frac{n_{j}}{\sigma^{2}}+\frac{1}{\tau^{2}}}}, \left [ \frac{n_{j}}{\sigma^{2}} +\frac{1}{\tau^{2}} \right ])$$
$$\frac{1}{\sigma^{2}}|\theta,D \sim \Gamma(\frac{1}{2}\left [ \nu_{0}+\sum_{j=1}^{m}n_{j} \right ], \frac{1}{2}\left [ \nu_{0}\sigma_{0}^{2}+\sum_{j=1}^{m}\sum_{i=1}^{n_{j}}(x_{i}^{(j)}-\theta_{j})^{2} \right ])$$

Substuting the given values for the prior parameters $\mu_{0}=7, \gamma^{2}_{0}=5, \tau^{2}_{0}=10, \eta_{0}=2, \sigma^{2}_{0}=15, \nu_{0}=2$ we can construct a Gibbs sampler in order to approximate the posterior distribution of $p(\boldsymbol{\theta}, \sigma^{2}, \mu,\tau^{2}|\boldsymbol{y_{1},...,y_{m}} )$

## (b) Gibbs sampler
The most important parts of the Gibbs sampler implementation in R are given below. The full listing of the code can be found in the Appendix.

```{r, warning=FALSE, echo=FALSE}
library(ggplot2)
library(coda)
library(lattice)
library(reshape2)
library(xtable)
library(png)
library(grid)

n<- sv <- ybar <- array(NA, 8)

Y1 <- read.table("/home/emarrig/repos/STK4021/project/school1.dat")
n[1] <- dim(Y1)[1]
sv[1] <- var(Y1)
ybar[1] <- mean(Y1$V1)

Y2 <- read.table("/home/emarrig/repos/STK4021/project/school2.dat")
n[2] <- dim(Y2)[1]
sv[2] <- var(Y2)
ybar[2] <- mean(Y2$V1)

Y3 <- read.table("/home/emarrig/repos/STK4021/project/school3.dat")
n[3] <- dim(Y3)[1]
sv[3] <- var(Y3)
ybar[3] <- mean(Y3$V1)

Y4 <- read.table("/home/emarrig/repos/STK4021/project/school4.dat")
n[4] <- dim(Y4)[1]
sv[4] <- var(Y4)
ybar[4] <- mean(Y4$V1)

Y5 <- read.table("/home/emarrig/repos/STK4021/project/school5.dat")
n[5] <- dim(Y5)[1]
sv[5] <- var(Y5)
ybar[5] <- mean(Y5$V1)

Y6 <- read.table("/home/emarrig/repos/STK4021/project/school6.dat")
n[6] <- dim(Y6)[1]
sv[6] <- var(Y6)
ybar[6] <- mean(Y6$V1)

Y7 <- read.table("/home/emarrig/repos/STK4021/project/school7.dat")
n[7] <- dim(Y7)[1]
sv[7] <- var(Y7)
ybar[7] <- mean(Y7$V1)

Y8 <- read.table("/home/emarrig/repos/STK4021/project/school8.dat")
n[8] <- dim(Y8)[1]
sv[8] <- var(Y8)
ybar[8] <- mean(Y8$V1)

Y <- read.csv("/home/emarrig/repos/STK4021/project/schools.csv", header = FALSE)
Y[is.na(Y)] <- 0
```
\marginnote{Initialization of variables and parameter chains.}
```{r, warning=FALSE}
m0 <- 7; g02 <- 5 ;t02 <- 10
eta0 <- 2; s02 <- 15; n0 <- 2
m <- 8 # 8 schools
K <- 5000 # Number of iterations

mus <- array(NA, dim=c(K,1))
taus <- array(NA, dim=c(K,1))
sigmas <- array(NA, dim=c(K,1))
thetas <- array(NA, dim=c(K,m))

# Initial values for the Gibbs sampler
thetas[1, ] <- ybar
mus[1] <- mean(thetas[1, ])
taus[1] <- var(thetas[1, ])
sigmas[1] <- mean(sv)

```
\marginnote{Main body of the for loop for the Gibbs sampler. The order with which the variables are updated is not important as long as the most current value of the other parameters is used.}
```{r, warning=FALSE}
for (k in 1:(K-1)) {
  
  # Sample mu
  denom <- m/taus[k]+1/g02
  mmu <- (m*mean(thetas[k, ])/taus[k] + m0/g02)/denom
  mus[k+1] <- rnorm (1, mmu, sqrt(1/denom))
  
  # Sample tau2
  taus[k+1] <-1/rgamma (1, (eta0+m)/2, eta0*t02 + sum((thetas[k, ]-mus[k+1])^2)/2)
  
  # Sample sigma
  ss <- n0*s02
  for (j in 1:m) {
    ss <- ss+sum( (Y[j, ] - thetas[k, j])^2)
  }
  sigmas[k+1] <- 1/rgamma (1,  (n0+sum(n))/2, ss/2)
  
  # Sample thetas
  for(j in 1:m) {
    denom <- (n[j]/sigmas[k+1]+1/taus[k+1])
    mtheta <- (ybar[j]*n[j] / sigmas[k+1]+mus[k+1]/taus[k+1] )/ denom
    thetas[k+1, j] <- rnorm(1, mtheta , sqrt(1/denom))
  }
}

```

Running the loop for `r K` iterations yields effective sample sizes of `r effectiveSize(mus)`, `r effectiveSize(sigmas)` and `r effectiveSize(taus)` for $\mu$, $\sigma^{2}$ and $\tau^{2}$ respectively.
The effective sample size for $\theta_{i}$'s is higher than 4000.

\marginnote{Calculating the posterior mean based on the values produced by the Gibbs sampler:}
```{r}
bayes.thetas <- array(NA, dim=c(8, 1))
for (i in 1:8) {
  bayes.thetas[i] <- mean(thetas[50:K, i])
}
bayes.mu <- mean(mus[50:K])
bayes.tau <- mean(taus[50:K])
bayes.sigma <- mean(sigmas[50:K])

```
```{r, echo=FALSE, fig.fullwidth = TRUE, fig.height=1, fig.cap="Trace plots for $\\mu, \\frac{1}{\\tau^{2}}$ and $\\frac{1}{\\sigma^{2}}$"}
img <- readPNG('/home/emarrig/repos/STK4021/project/traces1.png')
grid.raster(img)

```
```{r, echo=FALSE, fig.fullwidth = TRUE, fig.cap="Theta trace plots"}
img2 <- readPNG('/home/emarrig/repos/STK4021/project/traces2.png')
grid.raster(img2)

```


The chains in the traces appear that have achieved stationarity (or at least there is no indication for the opposite). The autocorrelations for $\mu, \sigma^{2}$ and $\tau^{2}$ are 0.02380184, 0.04287796 and 0.03068566 respectively which are quite low values.
\pagebreak

## (c) Comparison of prior and posterior densities
```{r, echo=FALSE, fig.height=1, fig.fullwidth = TRUE, fig.cap="Prior (red) and posterior(black) densities for $\\mu, \\frac{1}{\\tau^{2}}$ and $\\frac{1}{\\sigma^{2}}$"}
img <- readPNG('/home/emarrig/repos/STK4021/project/densities.png')
grid.raster(img)
```
The posterior means for $\mu, \sigma^{2}$ and $\tau^{2}$ are `r bayes.mu`, `r bayes.sigma` and `r bayes.tau` respectively. While the within the group variation ($\sigma^{2}$) has been concentrated around a quite small value, the expectation for between group variation ($\tau^{2}$) is an order of magnitude higher. The posterior expectation for $\mu$ has also increased and is closer to the total sample mean.


```{r, echo=FALSE}
df2 <- merge(Y1, Y2, all = T)
df2 <- merge(df2, Y3, all=T)
df2 <- merge(df2, Y4, all=T)
df2 <- merge(df2, Y5, all=T)
df2 <- merge(df2, Y6, all=T)
df2 <- merge(df2, Y7, all=T)
df2 <- merge(df2, Y8, all=T)

N <- dim(df2)[1]
```

## (d) Empirical Bayes estimates
The pooled sample variance of the data is calculated using the following formula:
$$\hat{\sigma} = \frac{\sum_{i=1}^{k}{(n_{i}-1)\sigma_{i}^{2}}}{\sum_{i=1}^{k}(n_{i}-1)}$$ which yields the value of `r sigma_hat`. This value shall be considered as the "known" $\sigma^{2}$ value for the empirical Bayes analysis. Based on the analysis in the notes [^notes2], pages 39 and 40, we can estimate $\mu, \tau^{2}$  using the sample mean $\hat{\mu} = \bar{y}$ and $\hat{\tau}^{2}=max(0, s^{2}-\sigma^{2})$ where $$s^{2}=\frac{1}{k}\sum_{i=1}^{k}(y_{i}-\bar{y})^{2}$$

Given the above calculations and the empirical framework, the thetas follow a normal distribution $$\hat{\theta_{i}^{\mu\tau}}|y_{i}, \hat{\mu} \sim N(\bar{y}\hat{\omega} + {(1-\hat{\omega})y_{i}}, (1-\hat{\omega})\sigma^{2})$$ where $\hat{\omega}=\frac{\sigma^{2}}{\sigma^{2}+\hat{\tau}^{2}}$.
\marginnote{sv is the within group variance in the sample and }
```{r}
# Pooled sample variance
sigma_hat <- sum((n-1)*sv)/sum(n)

# Sample mean
mu_hat <- sum(colSums(Y, na.rm=TRUE))/sum(n)

# Calculate s2 over all values
sigmas_hat <- sum((df2$V1-mu_hat)^2)/N

# Calculate tau2 
tau_hat <- max(0, sigmas_hat - sigma_hat)

# Calculate omega
omega_hat <- sigma_hat/(sigma_hat+tau_hat)

# Calculate thetas
emp.thetas <- array(NA, dim=c(m, 1))
for (i in 1:m) {
  #emp.thetas[i] <- mu_hat + (1-omega_hat)*(ybar[i] - mu_hat)
  emp.thetas[i] <- mu_hat*omega_hat + (1-omega_hat)*ybar[i]
}

```

The table below shows the different estimated values of the thetas. We can see that the Bayes estimates are pulled towards the sample mean $\hat{\mu}$ (7.6913) but the observed shrinkage is much smaller than the one observed in the Empirical Bayes (EB) estimates.

```{r, echo=FALSE, results='asis'}
thetas.df <- t(rbind(as.data.frame(ybar)$ybar, bayes.thetas[,1], emp.thetas[,1]))
colnames(thetas.df) <- c("Sample", "Bayes", "EB")

options(xtable.comment = FALSE)
options(xtable.booktabs = TRUE)
xtable(thetas.df, caption = "Thetas per group. The empirical Bayes values are more aggressively pulled towards the sample mean (7.6913) ")

```

\pagebreak

## (e) Plots and estimations

```{r, echo=FALSE}
# Prior calculation based on the given parameters
tt0 <- 1./rgamma(10000, 1, 10)
ss0 <- 1./rgamma(10000, 1, 15)

dfom <- as.data.frame(ss0/(tt0+ss0))
colnames(dfom) <- c("prior")

# Calculation based on the sampler
omegas = sigmas/(taus+sigmas)
omega_hat.post <- mean(omegas)
dfom2 <- as.data.frame(omegas)

```

```{r, echo=FALSE, fig.cap="Omega prior density(red), posterior (black), EB estimator (blue dashed line) and posterior mean estimate (green dashed line)"}
ggplot() + geom_density(aes(x=prior), colour="red", data=dfom) + 
  geom_density(aes(x=V1), colour="black", data=dfom2) +
  xlab(expression(omega)) + geom_vline(xintercept=omega_hat.post, col='green', linetype = "longdash") +
  geom_vline(xintercept=omega_hat, col='blue', linetype = "longdash") +
  xlim(0, 1.2)
```
As it can be seen from Figure 4, the value of $\bar{\omega}$, estimated using the EB framework is significantly higher than the expectation calculated using the standard Bayes framework. The reason for this difference is that in the standard Bayes framework the posterior expectation of $\tau^{2}$ is significantly higher than that of $\sigma^{2}$, indicating a significantly higher between group variation than the one within the groups. In the EB framework, since that value of $\sigma^{2}$ was chosen to be the pooled sample variance, it was much higher and closer to the estimated value for $\tau^{2}$. This difference in the two $\omega$ values is what drove the EB estimates for the group means closer to the sample mean $\hat{\mu}$.

## (f) Posterior probabilities for thetas
```{r, echo=FALSE}
bayes.res6 <- sum(thetas[, 7] < thetas[, 6])/K
bayes.res5 <- sum(thetas[, 7] < thetas[, 5])/K
bayes.res4 <- sum(thetas[, 7] < thetas[, 4])/K
bayes.res3 <- sum(thetas[, 7] < thetas[, 3])/K
bayes.res2 <- sum(thetas[, 7] < thetas[, 2])/K
bayes.res1 <- sum(thetas[, 7] < thetas[, 1])/K
bayes.res8 <- sum(thetas[, 7] < thetas[, 8])/K

bayes.all <- bayes.res1*bayes.res2*bayes.res3*bayes.res4*bayes.res5*bayes.res6*bayes.res8

emp.theta.d7 <-  rnorm(1000, emp.thetas[7], (1-omega_hat)*sigma_hat)
emp.theta.d6 <-  rnorm(1000, emp.thetas[6], (1-omega_hat)*sigma_hat)
emp.theta.d5 <-  rnorm(1000, emp.thetas[5], (1-omega_hat)*sigma_hat)
emp.theta.d4 <-  rnorm(1000, emp.thetas[4], (1-omega_hat)*sigma_hat)
emp.theta.d3 <-  rnorm(1000, emp.thetas[3], (1-omega_hat)*sigma_hat)
emp.theta.d2 <-  rnorm(1000, emp.thetas[2], (1-omega_hat)*sigma_hat)
emp.theta.d8 <-  rnorm(1000, emp.thetas[8], (1-omega_hat)*sigma_hat)
emp.theta.d1 <-  rnorm(1000, emp.thetas[1], (1-omega_hat)*sigma_hat)

emp.res6 <- sum(emp.theta.d7 < emp.theta.d6)/1000
emp.res5 <- sum(emp.theta.d7 < emp.theta.d5)/1000
emp.res4 <- sum(emp.theta.d7 < emp.theta.d4)/1000
emp.res3 <- sum(emp.theta.d7 < emp.theta.d3)/1000
emp.res2 <- sum(emp.theta.d7 < emp.theta.d2)/1000
emp.res1 <- sum(emp.theta.d7 < emp.theta.d1)/1000
emp.res8 <- sum(emp.theta.d7 < emp.theta.d8)/1000

emp.all <- emp.res1*emp.res2*emp.res3*emp.res4*emp.res5*emp.res6*emp.res8
```

The posterior probability of $\theta_{7}$ being smaller than $\theta_{6}$ using the Gibbs sampler is `r bayes.res6` and the same probability using the empirical bayes estimator is `r emp.res6`.
The posterior probability of $\theta_{7}$ being smaller than all of the other thetas is calculated from the product of all the respective probabilities (assuming independence). This estimate using the Gibbs sampler is `r bayes.all` and the respective one using the Empirical Bayes framework is `r emp.all`.

\pagebreak

# Problem 3 (linear regression)
# (a) Linear regression model

```{r, echo=FALSE}
library(MASS)
options(digits=4)

### sample from a multivariate normal
rmvnorm<-
function(n,mu,Sigma) {
  p<-length(mu)
  res<-matrix(0,nrow=n,ncol=p)
  if( n>0 & p>0 ) {
  E<-matrix(rnorm(n*p),n,p)
  res<-t(  t(E%*%chol(Sigma)) +c(mu))
                   }
  res
}
df <- read.table("/home/emarrig/repos/STK4021/project/crime.dat",
                 header=TRUE)
```
Fitting an Ordinary Least Squares (OLS) model is as simple as calling lm() function, as below

```{r}
ols <- lm(y~., data=df)
beta_ols  <- ols$coefficients
```
The Bayesian model using the g-prior assumes the following distributions for sigma and beta as can be senn in the notes[^notes3]:

$$\frac{1}{\sigma^{2}}|D,\boldsymbol{X} \sim \Gamma(\frac{\nu_{0}+n}{2}, \frac{\nu_{0}\sigma_{0}^{2}+SSR_{g}}{2})$$
$$\boldsymbol{\beta} \sim N_{p}(\frac{g}{g+1}\boldsymbol{\hat{\beta_{OLS}}}, \frac{g}{g+1}\sigma^{2(k)}\left [ \boldsymbol{X^{T}X} \right ]^{-1})$$
where $$SSR_{g}=\boldsymbol{y}^{T}(\boldsymbol{I}-\frac{g}{g+1}\boldsymbol{X(X^{T}X)^{-1}X^{T}})\boldsymbol{y}$$
Within the above framework and the given prior parameters we can construct a Monte Carlo sampler in order to sample the beta and sigma values.

The following function is the implementation of the above in R.
```{r}
g_prior <- function(n0, ss0, g, K, y, X, beta_ols){
  n <- dim(X)[1]
  
  # Calculate SSRg
  temp <- (g/(g+1))*X%*%solve(t(X)%*%X)%*%t(X)
  SSRg <- t(y)%*%(diag(1, nrow=n) - temp)%*%y
  
  # Initialize beta
  beta <- array(NA, dim=c(K,16))
  
  # Sample s2 
  s2 <- 1/rgamma (K, (n0+n)/2, (n0*ss0+SSRg)/2)
  
  # Sample beta
  mu <- beta_ols*g/(g+1)
  for (i in 1:K) {
    Sigma <- s2[i]*solve(t(X)%*%X)*g/(g+1)
    beta[i, ] <- rmvnorm(1, mu, Sigma)
  }
  beta
}

```


```{r, echo=FALSE}
n0 <- 2
ss0 <- 1
 
y <- df$y
X <- as.matrix(subset(df, select=-y))
X <- cbind(1,as.matrix(X)) 
n <- dim(X)[1]
g <- n
K <- 1000

betas <- g_prior(n0, ss0, g, K, y, X, beta_ols)

```

```{r, echo=FALSE}
# Get the posterior mean of betas
beta_g <- array(NA, dim=c(16, 1))
for (i in 1:16) {
  beta_g[i] <- mean(betas[, i])
}

```
```{r, echo=FALSE}
quant_g <- array(NA, dim=c(16, 2))
for (i in 1:16) {
  quant_g[i, ] <- quantile(betas[, i], c(0.025, 0.975))
}
```
The results for the coefficient estimates are summarized in Table 2. The values of the OLS estimates and the Bayes method estimates are quite close to each other but we have to bear in mind that the size of the dataset is not that big.

```{r, echo=FALSE, results='asis'}
dff <- rbind(beta_ols, t(beta_g), quant_g[1:16, 1], quant_g[1:16, 2])
dff <-t(dff)
colnames(dff) <- c('OLS', 'Bayes', '2.5%', '97.5%')

options(xtable.comment = FALSE)
options(xtable.booktabs = TRUE)
xtable(dff, caption = "Beta estimates from OLS and Bayes and Bayes quantiles")

```


## (b) Relationship between crime and explanatory variables
Based on the OLS analysis, the most statistically significant explanatory variables are M (percentage of males aged 14–24), Ed (mean years of schooling), U2 (unemployment rate of urban males 35–39), Ineq (income inequality) and Prob(probability of imprisonment).

## (c) Test and training split results using OLS
We now split the dataset into training and test sets with an almost 50-50 cut.
```{r}
# Split the dataset into train and test
sample_size <- floor(0.5 * nrow(df))

# Randomly select the training indices
# The rest are going to be the test indices
train_ind <- sample(seq_len(nrow(X)), size = sample_size)

df.train <- df[train_ind,]
df.test <- df[-train_ind,]

```

```{r}
# Run the standard OLS process on the training set
ols <- lm(y~., data=df.train)
beta_ols  <- ols$coefficients

# Predict in the test set and calculate the MSE
pred.OLS <-  predict(ols, newdata=df.test)
pred.mse <- mean( (df.test$y - pred.OLS)^2 )
```

```{r, echo=FALSE, fig.fullwidth = TRUE, fig.cap="Left: Predicted versus actual y values (OLS), Right: Residuals in Ordinary Least Squares method"}
img <- readPNG('/home/emarrig/repos/STK4021/project/predict1.png')
grid.raster(img)
#p1 <- qplot(pred.OLS, df.test$y, xlab='Predicted data', ylab='Actual data')+
#  geom_abline(intercept = 0, slope = 1, col='red')

#p2 <- qplot(seq(1, length(pred.OLS)-1), ols$residuals, ylab="Residuals", xlab="Index")
#multiplot(p1, p2, cols=2)
```
The Mean Square Error (MSE value) for the OLS method is `r pred.mse`.


## (d) Test and training split results using Bayes with g-prior

Similarly we repeat the procedure in (a) using only the training data to obtain the betas.
```{r}
# Repeat the process for the Bayes method with the g-prior
# We have to add a column of 1's in X
X <- as.matrix(subset(df.train, select=-y))
X <- cbind(1,as.matrix(X))

# Initialize g, K
n <- dim(X)[1]
g <- n
K <- 1000
betas <- g_prior(n0, ss0, g, K, df.train$y, X, beta_ols)

# Calculate the posterior means
beta_g <- array(NA, dim=c(1, 16))
for (i in 1:16) {
  beta_g[1, i] <- mean(betas[, i])
}
```
Using the rest of the data set for testing we obtain the prediction error for the Bayes method.
```{r}
# Predict using the test set
X.test <- as.matrix(subset(df.test, select=-y))
X.test <- cbind(1,as.matrix(X.test))
pred.bayes <- X.test%*%t(beta_g)
pred.mse.bayes <- mean( (df.test$y - pred.bayes)^2 )
```

The Mean Square Error (MSE value) for the Bayes method is `r pred.mse.bayes` and is consistently smaller than the respective error using the OLS method, as shall also be shown in the next part of the exercise.

```{r, echo=FALSE, fig.fullwidth = TRUE, fig.cap="Left: Predicted versus actual y values (Bayes), Right: Residuals in Bayes method"}
img <- readPNG('/home/emarrig/repos/STK4021/project/predict2.png')
grid.raster(img)
#p1 <- qplot(pred.OLS, df.test$y, xlab='Predicted data', ylab='Actual data') +
#  geom_abline(intercept = 0, slope = 1, col='red')
#bayes.residuals <- df.test$y-pred.bayes
#p2 <- qplot(seq(1, length(bayes.residuals)), bayes.residuals, ylab="Residuals", xlab="Index")
#multiplot(p1, p2, cols=2)
```


\pagebreak

## (e) Multiple repetitions
The same process is repeated 100 times with different training and test sets.
```{r, echo=FALSE}
# Repeat the process m times
m <- 100
pred.err<-matrix(NA, m, 2)
for (j in 1:m) {
  # Randomly select the training indices
  # The rest are used as test indices
  train_ind <- sample(seq_len(nrow(df)), size = sample_size)
  
  df.train <- df[train_ind,]
  df.test <- df[-train_ind,]
  
  # Run the standard OLS process on the training set
  ols <- lm(y~., data=df.train)
  beta_ols  <- ols$coefficients
  
  # Predict in the test set and calculate the MSE
  pred.OLS <-  predict(ols, newdata=df.test)
  pred.err[j,1] <- mean( (df.test$y - pred.OLS)^2 )
  
  # Repeat the process for the Bayes method with the g-prior
  # We have to add a column of 1's in X
  X <- as.matrix(subset(df.train, select=-y))
  X <- cbind(1,as.matrix(X))
  
  # Initialize g, K
  n <- dim(X)[1]
  g <- n
  K <- 1000
  betas <- g_prior(n0, ss0, g, K, df.train$y, X, beta_ols)
  
  # Calculate the posterior means
  beta_g <- array(NA, dim=c(1, 16))
  for (i in 1:16) {
    beta_g[1, i] <- mean(betas[, i])
  }
  
  # Predict using the test set
  X.test <- as.matrix(subset(df.test, select=-y))
  X.test <- cbind(1,as.matrix(X.test))
  pred.bayes <- X.test%*%t(beta_g)
  pred.err[j, 2] <- mean( (df.test$y - pred.bayes)^2 )
}

```

```{r, echo=FALSE}
#mean(pred.err[, 1]) # OLS
#mean(pred.err[, 2]) # Bayes
#boxplot(pred.err[, 1], pred.err[, 2])
err <- as.data.frame(pred.err)
names(err) <- c("OLS", "Bayes")

```
The mean MSE using the OLS method is `r mean(pred.err[, 1])` and that of the Bayes method is `r mean(pred.err[, 2])`.
```{r, echo=FALSE, warning=FALSE, error=FALSE, fig.cap="Boxplots of MSE"}
ggplot(data = melt(err), aes(x=variable, y=value)) + geom_boxplot(aes(fill=variable)) +
  ylab("MSE") + xlab("Method")

```

```{r, echo=FALSE, fig.cap="MSE for the two methods, Red=OLS, Blue=Bayes"}
ggplot() +
  geom_line(data=err, aes(x=seq_along(err$OLS), y=err$OLS), col='red') +
  geom_line(data=err, aes(x=seq_along(err$OLS), y=err$Bayes), col='blue') +
  xlab("Iteration") + ylab("MSE")
```
Both the boxplot and the MSE plot suggest that the Bayes method has a slightly smaller mean error and this has been observed consistently. Given the fact that the dataset was already small to begin with and with the 50-50 split between training and testing it is sensible that the MSEs in the two methods are not that far off.


\pagebreak

# Appendix

## Problem 1 code
```{r, eval=FALSE}
library(ggplot2)
library(coda)
library(xtable)
# Set prior parameters
m0 <- 7
g02 <- 5
t02 <- 10
eta0 <- 2
s02 <- 15
n0 <- 2

n<- sv <- ybar <- array(NA, 8)

Y1 <- read.table("/home/emarrig/repos/STK4021/project/school1.dat")
n[1] <- dim(Y1)[1]
sv[1] <- var(Y1)
ybar[1] <- mean(Y1$V1)

Y2 <- read.table("/home/emarrig/repos/STK4021/project/school2.dat")
n[2] <- dim(Y2)[1]
sv[2] <- var(Y2)
ybar[2] <- mean(Y2$V1)

Y3 <- read.table("/home/emarrig/repos/STK4021/project/school3.dat")
n[3] <- dim(Y3)[1]
sv[3] <- var(Y3)
ybar[3] <- mean(Y3$V1)

Y4 <- read.table("/home/emarrig/repos/STK4021/project/school4.dat")
n[4] <- dim(Y4)[1]
sv[4] <- var(Y4)
ybar[4] <- mean(Y4$V1)

Y5 <- read.table("/home/emarrig/repos/STK4021/project/school5.dat")
n[5] <- dim(Y5)[1]
sv[5] <- var(Y5)
ybar[5] <- mean(Y5$V1)

Y6 <- read.table("/home/emarrig/repos/STK4021/project/school6.dat")
n[6] <- dim(Y6)[1]
sv[6] <- var(Y6)
ybar[6] <- mean(Y6$V1)

Y7 <- read.table("/home/emarrig/repos/STK4021/project/school7.dat")
n[7] <- dim(Y7)[1]
sv[7] <- var(Y7)
ybar[7] <- mean(Y7$V1)

Y8 <- read.table("/home/emarrig/repos/STK4021/project/school8.dat")
n[8] <- dim(Y8)[1]
sv[8] <- var(Y8)
ybar[8] <- mean(Y8$V1)

Y <- read.csv("/home/emarrig/repos/STK4021/project/schools.csv", header = FALSE)
Y[is.na(Y)] <- 0

m <- 8 # 8 schools
K <- 5000 # Number of iterations

mus <- array(NA, dim=c(K,1))
taus <- array(NA, dim=c(K,1))
sigmas <- array(NA, dim=c(K,1))
thetas <- array(NA, dim=c(K,m))

# Initial values for the sampler
thetas[1, ] <- ybar
mus[1] <- mean(thetas[1, ])
taus[1] <- var(thetas[1, ])
sigmas[1] <- mean(sv)

for (k in 1:(K-1)) {
  
  # Sample mu
  denom <- m/taus[k]+1/g02
  mmu <- (m*mean(thetas[k, ])/taus[k] + m0/g02)/denom
  mus[k+1] <- rnorm (1, mmu, sqrt(1/denom))
  
  # Sample tau2
  taus[k+1] <-1/rgamma (1, (eta0+m)/2, eta0*t02 + sum((thetas[k, ]-mus[k+1])^2)/2)
  
  # Sample sigma
  ss <- n0*s02
  for (j in 1:m) {
    ss <- ss+sum( (Y[j, k] - thetas[k, j])^2)
  }
  sigmas[k+1] <- 1/rgamma (1,  (n0+sum(n))/2, ss/2)
  
  # Sample thetas
  for(j in 1:m) {
    denom <- (n[j]/sigmas[k+1]+1/taus[k+1])
    mtheta <- (ybar[j]*n[j] / sigmas[k+1]+mus[k+1]/taus[k+1] )/ denom
    thetas[k+1, j] <- rnorm(1, mtheta , sqrt(1/denom))
  }
}

bayes.thetas <- array(NA, dim=c(8, 1))
for (i in 1:8) {
  bayes.thetas[i] <- mean(thetas[, i])
}
# Posterior means
bayes.mu <- mean(mus)
bayes.tau <- mean(taus)
bayes.sigma <- mean(sigmas)

# 95% intervals
quantile(mus,c(0.025,0.975)) 
quantile(taus,c(0.025,0.975))
quantile(sigmas,c(0.025,0.975))


p1 <- qplot(seq(1, length(mus[50:K])), mus[50:K], xlab="Iteration", ylab=expression(mu))
p2 <- qplot(seq(1, length(sigmas[50:K])), sigmas[50:K], xlab="Iteration", ylab=expression(sigma^2))
p3 <- qplot(seq(1, length(taus[50:K])), taus[50:K], xlab="Iteration", ylab=expression(tau^2))
multiplot(p1, p2, p3, cols=3)

p1 <- qplot(seq(1, length(thetas[, 1])), thetas[, 1], xlab="Iteration", ylab="theta1")
p2 <- qplot(seq(1, length(thetas[, 2])), thetas[, 2], xlab="Iteration", ylab="theta2")
p3 <- qplot(seq(1, length(thetas[, 3])), thetas[, 3], xlab="Iteration", ylab="theta3")
p4 <- qplot(seq(1, length(thetas[, 4])), thetas[, 4], xlab="Iteration", ylab="theta4")
p5 <- qplot(seq(1, length(thetas[, 5])), thetas[, 5], xlab="Iteration", ylab="theta5")
p6 <- qplot(seq(1, length(thetas[, 6])), thetas[, 6], xlab="Iteration", ylab="theta6")
p7 <- qplot(seq(1, length(thetas[, 7])), thetas[, 7], xlab="Iteration", ylab="theta7")
p8 <- qplot(seq(1, length(thetas[, 8])), thetas[, 8], xlab="Iteration", ylab="theta8")
multiplot(p1, p2, p3, p4, p5, p6, p7, p8, cols=2)


# Check autocorrelation

par(mfrow=c(1,3))
acf(mus) -> a1
acf(sigmas) -> a2
acf(taus) -> a3

# Check effective size 
effectiveSize(mus)
effectiveSize(sigmas)
effectiveSize(taus)
effectiveSize(thetas[, 1])
effectiveSize(thetas[, 2])
effectiveSize(thetas[, 3])
effectiveSize(thetas[, 4])
effectiveSize(thetas[, 5])
effectiveSize(thetas[, 6])
effectiveSize(thetas[, 7])
effectiveSize(thetas[, 8])

par(mfrow=c(4,2))
plot(thetas[, 1], type="l", xlab="Iteration", ylab="theta1")
plot(thetas[, 2], type="l", xlab="Iteration", ylab="theta2")
plot(thetas[, 3], type="l", xlab="Iteration", ylab="theta3")
plot(thetas[, 4], type="l", xlab="Iteration", ylab="theta4")
plot(thetas[, 5], type="l", xlab="Iteration", ylab="theta5")
plot(thetas[, 6], type="l", xlab="Iteration", ylab="theta6")
plot(thetas[, 7], type="l", xlab="Iteration", ylab="theta7")
plot(thetas[, 8], type="l", xlab="Iteration", ylab="theta8")

plot(density(thetas[, 1]))
plot(density(thetas[, 2]))
plot(density(thetas[, 3]))
plot(density(thetas[, 4]))
plot(density(thetas[, 5]))
plot(density(thetas[, 6]))
plot(density(thetas[, 7]))
plot(density(thetas[, 8]))

acf(mus) -> a1
acf(sigmas) -> a2
acf(taus) -> a3
mean(a1$acf)
mean(a2$acf)
mean(a3$acf)

# Code for question (c)
par(mfrow=c(1,1))
# Plot densities for prior and posterior
mu.prior <- rnorm(10000, m0, sqrt(5))
dmf <- as.data.frame(mus[50:K])
dmf2 <- as.data.frame(mu.prior)
names(dmf) <- c("mus")

p1 <- ggplot() + geom_density(aes(x=mu.prior), colour="red", data=dmf2) + 
  geom_density(aes(x=mus), colour="black", data=dmf) +
  xlab(expression(mu)) + geom_vline(xintercept=bayes.mu, col='gray', linetype = "longdash")

tau.prior <- 1./rgamma(10000, 1, eta0*t02/2)
dmf <- as.data.frame(taus[50:K])
dmf2 <- as.data.frame(tau.prior)
names(dmf) <- c("taus")

p2 <- ggplot() + geom_density(aes(x=1/tau.prior), colour="red", data=dmf2) + 
  geom_density(aes(x=1/taus), colour="black", data=dmf) +
  xlab(expression(1/tau^2)) + geom_vline(xintercept=1./bayes.tau, col='gray', linetype = "longdash")

sigma.prior <- 1./rgamma(10000, 1, n0*s02/2)
dmf <- as.data.frame(sigmas[50:K])
dmf2 <- as.data.frame(sigma.prior)
names(dmf) <- c("sigmas")

p3 <- ggplot() + geom_density(aes(x=1/sigma.prior), colour="red", data=dmf2) + 
  geom_density(aes(x=1/sigmas), colour="black", data=dmf) +
  xlab(expression(1/sigma^2)) + geom_vline(xintercept=1./bayes.sigma, col='gray', linetype = "longdash")

multiplot(p1, p2, p3, cols=3)

## Code for question (d)

df2 <- merge(Y1, Y2, all = T)
df2 <- merge(df2, Y3, all=T)
df2 <- merge(df2, Y4, all=T)
df2 <- merge(df2, Y5, all=T)
df2 <- merge(df2, Y6, all=T)
df2 <- merge(df2, Y7, all=T)
df2 <- merge(df2, Y8, all=T)

N <- dim(df2)[1]

# Pooled population variance
sigma_hat <- sum((n-1)*sv)/sum(n)

# Population mean
mu_hat <- sum(colSums(Y, na.rm=TRUE))/sum(n)

# Calculate s2 over all values
sigmas_hat <- sum((df2$V1-mu_hat)^2)/N

# Calculate tau2 
tau_hat <- max(0, sigmas_hat - sigma_hat)

# Calculate omega_hat
omega_hat <- sigma_hat/(sigma_hat+tau_hat)

# Calculate thetas
emp.thetas <- array(NA, dim=c(m, 1))
for (i in 1:m) {
  emp.thetas[i] <- mu_hat + (1-omega_hat)*(ybar[i] - mu_hat)
}

# Create the table for the report
thetas.df <- t(rbind(as.data.frame(ybar)$ybar, bayes.thetas[,1], emp.thetas[,1]))
colnames(thetas.df) <- c("Sample", "Bayes", "EB")
options(xtable.comment = FALSE)
options(xtable.booktabs = TRUE)
xtable(thetas.df, caption = "Beta estimates from OLS and Bayes and Bayes quantiles")

## Code for question (e)

# Prior calculation based on the given parameters
tt0 <- 1./rgamma(10000, 1, 10)
ss0 <- 1./rgamma(10000, 1, 15)

dfom <- as.data.frame(ss0/(tt0+ss0))
colnames(dfom) <- c("prior")

# Calculation based on the sampler
omegas = sigmas/(taus+sigmas)
plot(density(omegas))
omega_hat.post <- mean(omegas)
dfom2 <- as.data.frame(omegas)

ggplot() + geom_density(aes(x=prior), colour="red", data=dfom) + 
  geom_density(aes(x=V1), colour="black", data=dfom2) +
  xlab(expression(omega)) + geom_vline(xintercept=omega_hat.post, col='green', linetype = "longdash") +
  geom_vline(xintercept=omega_hat, col='blue', linetype = "longdash") +
  xlim(0, 1.2)

## Code for question (f)

bays.res6 <- sum(thetas[, 7] < thetas[, 6])/K
bays.res5 <- sum(thetas[, 7] < thetas[, 5])/K
bays.res4 <- sum(thetas[, 7] < thetas[, 4])/K
bays.res3 <- sum(thetas[, 7] < thetas[, 3])/K
bays.res2 <- sum(thetas[, 7] < thetas[, 2])/K
bays.res1 <- sum(thetas[, 7] < thetas[, 1])/K
bays.res8 <- sum(thetas[, 7] < thetas[, 8])/K

bays.all <- bays.res1*bays.res2*bays.res3*bays.res4*bays.res5*bays.res6*bays.res8

emp.theta.d7 <-  rnorm(1000, emp.thetas[7], (1-omega_hat)*sigma_hat)
emp.theta.d6 <-  rnorm(1000, emp.thetas[6], (1-omega_hat)*sigma_hat)
emp.theta.d5 <-  rnorm(1000, emp.thetas[5], (1-omega_hat)*sigma_hat)
emp.theta.d4 <-  rnorm(1000, emp.thetas[4], (1-omega_hat)*sigma_hat)
emp.theta.d3 <-  rnorm(1000, emp.thetas[3], (1-omega_hat)*sigma_hat)
emp.theta.d2 <-  rnorm(1000, emp.thetas[2], (1-omega_hat)*sigma_hat)
emp.theta.d8 <-  rnorm(1000, emp.thetas[8], (1-omega_hat)*sigma_hat)
emp.theta.d1 <-  rnorm(1000, emp.thetas[1], (1-omega_hat)*sigma_hat)

emp.res6 <- sum(emp.theta.d7 < emp.theta.d6)/1000
emp.res5 <- sum(emp.theta.d7 < emp.theta.d5)/1000
emp.res4 <- sum(emp.theta.d7 < emp.theta.d4)/1000
emp.res3 <- sum(emp.theta.d7 < emp.theta.d3)/1000
emp.res2 <- sum(emp.theta.d7 < emp.theta.d2)/1000
emp.res1 <- sum(emp.theta.d7 < emp.theta.d1)/1000
emp.res8 <- sum(emp.theta.d7 < emp.theta.d8)/1000

emp.all <- emp.res1*emp.res2*emp.res3*emp.res4*emp.res5*emp.res6*emp.res8

# Multiple plot function
#
# ggplot objects can be passed in ..., or to plotlist (as a list of ggplot objects)
# - cols:   Number of columns in layout
# - layout: A matrix specifying the layout. If present, 'cols' is ignored.
#
# If the layout is something like matrix(c(1,2,3,3), nrow=2, byrow=TRUE),
# then plot 1 will go in the upper left, 2 will go in the upper right, and
# 3 will go all the way across the bottom.
#
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  require(grid)
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}

```

\pagebreak

## Problem 2 code

```{r, eval=FALSE}
library(MASS)
library(ggplot2)
library(reshape2)
### sample from a multivariate normal
rmvnorm<-
  function(n,mu,Sigma) {
    p<-length(mu)
    res<-matrix(0,nrow=n,ncol=p)
    if( n>0 & p>0 ) {
      E<-matrix(rnorm(n*p),n,p)
      res<-t(  t(E%*%chol(Sigma)) +c(mu))
    }
    res
  }

g_prior <- function(n0, ss0, g, K, y, X, beta_ols){
  n <- dim(X)[1]
  # Calculate SSRg
  temp <- (g/(g+1))*X%*%solve(t(X)%*%X)%*%t(X)
  SSRg <- t(y)%*%(diag(1, nrow=n) - temp)%*%y
  
  # Initialize beta
  beta <- array(NA, dim=c(K,16))
  
  # Sample s2 
  s2 <- 1/rgamma (K, (n0+n)/2, (n0*ss0+SSRg)/2)
  
  # Sample beta
  mu <- beta_ols*g/(g+1)
  for (i in 1:K) {
    Sigma <- s2[i]*solve(t(X)%*%X)*g/(g+1)
    beta[i, ] <- rmvnorm(1, mu, Sigma)
  }
  beta
}

df <- read.table("/home/emarrig/repos/STK4021/project/crime.dat",
                 header=TRUE)
#write.csv(df, "crime.csv",)

ols <- lm(y~., data=df)
beta_ols  <- ols$coefficients
summary(ols)

n0 <- 2
ss0 <- 1
 
y <- df$y
X <- as.matrix(subset(df, select=-y))
X <- cbind(1,as.matrix(X)) 
n <- dim(X)[1]
g <- n
K <- 1000

# Obtain the betas
betas <- g_prior(n0, ss0, g, K, y, X, beta_ols)

# Get the posterior mean of betas
beta_g <- array(NA, dim=c(16, 1))
for (i in 1:16) {
  beta_g[i] <- mean(betas[, i])
}
beta_ols
t(beta_g)

quant_g <- array(NA, dim=c(16, 2))
for (i in 1:16) {
  quant_g[i, ] <- quantile(betas[, i], c(0.025, 0.975))
}
quant_g
dff <- rbind(beta_ols, t(beta_g), quant_g[1:16, 1], quant_g[1:16, 2])
dff <-t(dff)
colnames(dff) <- c('OLS', 'Bayes', '2.5%', '97.5%')

options(xtable.comment = FALSE)
options(xtable.booktabs = TRUE)
xtable(dff, caption = "Beta estimates from OLS and Bayes and Bayes quantiles")

# Code for question (c)

# Split the dataset into train and test
sample_size <- floor(0.5 * nrow(df))

# Randomly select the training indices
# The rest are going to be the test indices
train_ind <- sample(seq_len(nrow(X)), size = sample_size)

df.train <- df[train_ind,]
df.test <- df[-train_ind,]

# Run the standard OLS process on the training set
ols <- lm(y~., data=df.train)
beta_ols  <- ols$coefficients

# Predict in the test set and calculate the MSE
pred.OLS <-  predict(ols, newdata=df.test)
pred.mse <- mean( (df.test$y - pred.OLS)^2 )
p1 <- qplot(pred.OLS, df.test$y, xlab='Predicted data', ylab='Actual data')+
  geom_abline(intercept = 0, slope = 1, col='red')

p2 <- qplot(seq(1, length(pred.OLS)-1), ols$residuals, ylab="Residuals", xlab="Index")
multiplot(p1, p2, cols=2) # load multiplot from the previous section

# Code for question (d)

# Repeat the process for the Bayes method with the g-prior
# We have to add a column of 1's in X
X <- as.matrix(subset(df.train, select=-y))
X <- cbind(1,as.matrix(X))

# Initialize g, K
n <- dim(X)[1]
g <- n
K <- 1000
betas <- g_prior(n0, ss0, g, K, df.train$y, X)

# Calculate the posterior means
beta_g <- array(NA, dim=c(1, 16))
for (i in 1:16) {
  beta_g[1, i] <- mean(betas[, i])
}
#beta_g
#beta_ols

# Predict using the test set
X.test <- as.matrix(subset(df.test, select=-y))
X.test <- cbind(1,as.matrix(X.test))
pred.bayes <- X.test%*%t(beta_g)
pred.mse.bayes <- mean( (df.test$y - pred.bayes)^2 )
plot(pred.bayes, df.test$y)

pred.mse
pred.mse.bayes

bayes.residuals <- df.test$y-pred.bayes
p1 <- qplot(pred.OLS, df.test$y, xlab='Predicted data', ylab='Actual data') +
  geom_abline(intercept = 0, slope = 1, col='red')

p2 <- qplot(seq(1, length(bayes.residuals)), bayes.residuals, ylab="Residuals", xlab="Index")
multiplot(p1, p2, cols=2) # load multiplot from the previous section

# Repeat the process m times
m <- 80
pred.err<-matrix(NA,m, 2)
for (j in 1:m) {
  # Randomly select the training indices
  # The rest are going to be the test indices
  train_ind <- sample(seq_len(nrow(df)), size = sample_size)
  
  df.train <- df[train_ind,]
  df.test <- df[-train_ind,]
  
  # Run the standard OLS process on the training set
  ols <- lm(y~., data=df.train)
  beta_ols  <- ols$coefficients
  
  # Predict in the test set and calculate the MSE 
  pred.OLS <-  predict(ols, newdata=df.test)
  pred.err[j,1] <- mean( (df.test$y - pred.OLS)^2 )
  
  # Repeat the process for the Bayes method with the g-prior
  # We have to add a column of 1's in X
  X <- as.matrix(subset(df.train, select=-y))
  X <- cbind(1,as.matrix(X))
  
  # Initialize g, K
  n <- dim(X)[1]
  g <- n
  K <- 1000
  betas <- g_prior(n0, ss0, g, K, df.train$y, X, beta_ols)
  
  # Calculate the posterior means
  beta_g <- array(NA, dim=c(1, 16))
  for (i in 1:16) {
    beta_g[1, i] <- mean(betas[, i])
  }
  #beta_g
  #beta_ols
  
  # Predict using the test set
  X.test <- as.matrix(subset(df.test, select=-y))
  X.test <- cbind(1,as.matrix(X.test))
  pred.bayes <- X.test%*%t(beta_g)
  pred.err[j, 2] <- mean( (df.test$y - pred.bayes)^2 )
}

mean(pred.err[, 1]) # OLS
mean(pred.err[, 2]) # Bayes
#boxplot(pred.err[, 1], pred.err[, 2])

err <- as.data.frame(pred.err)
names(err) <- c("OLS", "Bayes")

# MSE boxplots
ggplot(data = melt(err), aes(x=variable, y=value)) + geom_boxplot(aes(fill=variable)) +
  ylab("MSE") + xlab("Method")

# MSE plot
ggplot() +
  geom_line(data=err, aes(x=seq_along(err$OLS), y=err$OLS), col='red') +
  geom_line(data=err, aes(x=seq_along(err$OLS), y=err$Bayes), col='blue') +
  xlab("Iteration") + ylab("MSE")

```

[^notes]: Example 12.2 (hierarchical normal model)
[^notes2]: Example 14.1
[^notes3]: Example 11.4 (g-prior)










