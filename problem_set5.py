import numpy as np 
import matplotlib.pyplot as plt
import utils_bayes as utils 

x = np.array([33, 14, 27, 90, 12, 17.])
y = np.array([1, 3, 2, 12, 1, 1.0])

# Initialization
k = 60000
alphas = np.zeros(k, dtype=float)
betas = np.zeros(k, dtype=float)
thetas = np.zeros((k, 6), dtype=float)

a = np.random.gamma(1.0, 1.0, 1)
b = np.random.gamma(10.0, 1.0, 1)

accept_ratio = 0.

# delta values
delta_a = 0.05
delta_b = 1.25  # 0.05

# Metropolis Hastings using a Uniform proposal [a-d, a+d]
for i in range(k):
    if i%1000 == 0:
        print i
    for j  in range(6):
        thetas[i, j] = np.random.gamma(y[j]+a, 1.0/(x[j]+b), 1)

    a_new = abs(np.random.uniform(a-delta_a, a+delta_a, 1))
    b_new = abs(np.random.uniform(b-delta_b, b+delta_b, 1))
    log_r = (a_new - a)*(np.sum(np.log(thetas[i,:])) - 1) - (b_new - b)*(np.sum(thetas[i,:]) + 1)+ 9*(np.log(b_new) - np.log(b))
    u = np.random.uniform(0, 1, 1)
    if np.log(u) < log_r:
        a = a_new
        b = b_new
        accept_ratio += 1
    alphas[i] = a
    betas[i] = b


print ("Acceptance ratio: %.2f" % (accept_ratio/k*100))
print ("Effective size of A: %d" % utils.effectiveSampleSize(alphas))
print ("Effective size of B: %d" % utils.effectiveSampleSize(betas))
print ("Effective size of theta: %d" % utils.effectiveSampleSize(thetas[:, 1]))

#plt.style.use('fivethirtyeight')
#print plt.style.available
# Plot the chains
plt.figure(1)
plt.subplot(311)
plt.plot(thetas[:, 1], c='black')

plt.subplot(312)
plt.plot(alphas, c='black')

plt.subplot(313)
plt.plot(betas, c='black')
plt.show()


for i in range(6):
    plt.subplot(6, 1, i+1)
    plt.hist(thetas[10000:, i])
    plt.vlines(y[i]/x[i], 0 , 40000)

plt.show()

a_prior = np.random.gamma(1, 1., 5000)
b_prior = np.random.gamma(10., 1., 5000)

from scipy import stats
density = stats.kde.gaussian_kde(a_prior/b_prior)
xs = np.linspace(0, 1.6, 200)
plt.hist(a_prior/b_prior, normed=True)
plt.plot(xs, density(xs))

plt.vlines(np.mean(y/x), 0, 7)
plt.title('Histogram of A/B')
plt.show()

for i in [1, 3, 4, 5, 6]:
    plt.scatter(thetas[:10000, 1], thetas[:10000, i-1], c='black')
    plt.plot([0., 0.8], [0., 0.8], c='red')
    plt.show()
